package ru.kev.copyfile;

/**
 * Класс в котором происходит запуск потоков.
 * И параллейное,последовательно и одиночное копирование файлов одинакового размера
 * в один файл.
 *
 * @author Karnauhov Evgeniy , 15IT18.
 */
public class Demo {
    public static void main(String[] args) throws InterruptedException {
        CopyFile file = new CopyFile("C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\Источник.txt", "C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\ПриёмФайлов.txt");
        CopyFile file1 = new CopyFile("C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\Источник.txt", "C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\ПриёмФайлов2.txt");
        sequence(file,file1);
        file= new CopyFile("C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\Источник.txt", "C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\ПриёмФайлов2.txt");
        file1=new CopyFile("C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\Источник.txt", "C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\ПриёмФайлов2.txt");
        parallel(file,file1);
        file=new CopyFile("C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\Источник.txt", "C:\\Users\\HP\\IdeaProjects\\Thread\\src\\ru\\kev\\copyfile\\ПриёмФайлов2.txt");
        single1(file);

    }

    public static void sequence(CopyFile source,CopyFile receiver) throws InterruptedException {
        long posled1 = System.currentTimeMillis();
        source.start();
        if (source.isAlive()) {
            source.join();
        }
        receiver.start();
        if (receiver.isAlive()) {
            receiver.join();
        }

        System.out.println("Время выполнения последовательно, первого потока : " + source.getTime());
        System.out.println("Время выполнения последовательно, второго потока : " + receiver.getTime());
        long posled = System.currentTimeMillis() - posled1;
        System.out.println("время последовательного потока: " +posled);
    }

    public static void parallel(CopyFile file,CopyFile file1) throws InterruptedException {
        long parall = System.currentTimeMillis();
        file.start();
        file1.start();
        file.join();
        file1.join();
        System.out.println("Время выполнения параллельно с первым потоком : " + file.getTime());
        System.out.println("Время выполнения параллельно с вторым потоком : " + file1.getTime());
        long parall1 =System.currentTimeMillis() - parall;
        System.out.println("время паралл. потока: "+parall1);
    }
    public static void single1(CopyFile file) throws InterruptedException {
        file.start();
        file.join();
        System.out.println("Время выполнения одного копирования файла : " + file.getTime());
    }
}
