package ru.kev.copyfile;

import java.io.*;

/**
 * Класс в котором происходит считывание с файла и запись в файл.
 *
 * @author Karnauhov Evgeniy , 15IT18.
 */
public class CopyFile extends Thread {

    private String source;
    private String receiver;
    private long time;

    public CopyFile(String source, String receiver) {
        this.source = source;
        this.receiver = receiver;
    }

    public long getTime() {
        return time;
    }

    public void run() {
        String string;
        long before = System.currentTimeMillis();
        try (BufferedReader reader = new BufferedReader(new FileReader(source));
        BufferedWriter writer = new BufferedWriter(new FileWriter(receiver,false))) {
            while ((string = reader.readLine())!=null){
                writer.write(string + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        time=System.currentTimeMillis() - before;

    }
}