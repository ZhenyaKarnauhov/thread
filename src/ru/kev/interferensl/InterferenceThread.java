package ru.kev.interferensl;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * Класс потоков.
 *
 * @author Evgeniy Karnauhov 15ИТ18
 */
public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private static volatile int i;

    public InterferenceThread(String name, InterferenceExample checker) {
        super(name);
        this.checker = checker;
    }

    public void run() {
        System.out.println(this.getName() + " начался");
        while (!checker.stop()) {
            increment();
        }
        System.out.println(this.getName() + " закончился");
    }

    /**
     * Метод для инкремента переменной i.
     */
    private static synchronized void increment() {
        i++;
    }

    public int getI() {
        return i;
    }
}