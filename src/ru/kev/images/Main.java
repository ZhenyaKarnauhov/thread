package ru.kev.images;

/**
 * Класс, в котором реализовано выполнение потока где находятся ссылки для скачивания.
 * "Качаем картинки".
 *
 * @author Evgeniy Karnauhov 15ИТ18
 */
public class Main {

    private static final String OUTPUT_FILE_TXT = "src\\ru\\kev\\images\\outFile.txt";

    public static void main(String[] args) throws InterruptedException {

        Image thread = new Image(OUTPUT_FILE_TXT);
        thread.start();
    }
}
