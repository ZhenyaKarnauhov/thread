package ru.kev.threadcurrent;

/**
 * Класс , демонстрирующий использование
 * метода класса Thread
 * в главном потоке программы
 *
 * @author Evgeniy Karnauhov 15IT18.
 */
public class ThreadDemo {
    public static void main(String[] args) {
        // переменная thread ссылается на главный поток программы
        Thread thread = Thread.currentThread();
        // вывод сведений о главном потоке
        System.out.println("Текущий поток : " +thread);
        System.out.println("Имя потока : " + thread.getName());
        System.out.println("Приоритет потока : " +thread.getPriority());
        System.out.println("Группа потока : " +thread.getThreadGroup());
        System.out.println("Идентификатор потока : " +thread.getId());
        System.out.println("Состояние потока : " +thread.getState());
        thread.setName("Главный поток.");
        thread.setPriority(10);
        System.out.println("Теперь текущий поток: " +thread);

        // Вывод цифры с задержкой потока на 1 секунду
        for (int i=3;i>0;i--) {
            System.out.println(i);
            try {
                Thread.sleep(300);
            }catch (InterruptedException e){
                System.out.println("Поток завершён.");
            }
        }
    }
}
