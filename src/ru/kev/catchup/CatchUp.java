package ru.kev.catchup;

/**
 * Класс "Догонялки".
 * В котором реализованно динамическое изменение приоритетов двух потоков.
 */
public class CatchUp extends Thread {

    private int first;
    private int second;
    private String name;
    public CatchUp(int first, int second,String name) {
        this.first = first;
        this.second = second;
        this.name = name;
    }
    public void run() {
        setPriority(first);
        for (int i = 0; i < 5000; i++) {
            try {sleep(10);}
            catch (InterruptedException e) {
            }
            System.out.println(name + i);
            if (i == 500) {
                setPriority(second);
            }
        }
    }
}



