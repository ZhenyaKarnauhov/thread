package ru.kev.catchup;

/**
 * Created by HP on 23.12.2017.
 */
public class Main {
    public static void main (String[]args) {
        CatchUp thread1 = new CatchUp(10,1,"Поток1:");
        CatchUp thread2 = new CatchUp(1,10,"Поток2:");
        thread1.start();
        thread2.start();
    }
}
