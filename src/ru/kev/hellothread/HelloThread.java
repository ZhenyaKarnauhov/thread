package ru.kev.hellothread;

/**
 * Создание класса Thread
 *
 * Класс Thread сам реализует интерфейс Runnable
 * хотя его метод run() ничего не делает
 * Подкласс класса Thread может
 * облегчить собственную реализацию метода run()
 *
 * @author Evgeniy Karnauhov.
 */
public class HelloThread extends Thread {
    public void run(){
        System.out.println("Hello from a thread!");
    }

    public static void main(String[] args) {
        (new HelloThread()).start();
        System.out.println("Hello from main thread!");
    }
}
