package ru.kev.hellorunnable;

/**
 * Реализация интерфейса Runnable
 *
 * Интерфейс Runnable определяет один метод run,
 * предназначенный для размещения кода , исполняемого в потоке.
 * Runnable-объект пересылается в конструктор Thread
 * и с помощью метода start() поток запускается
 */
public class Hellorunnable implements Runnable {
    public void run() {
        for (int i=3;i>0;i--) {
            try {
                Thread.sleep(3000);
                System.out.println("Первый поток!");
            }catch (InterruptedException e){
            }
        }
        System.out.println("Hello from a thread!");
    }

    public static void main(String[] args) {
        for (int i=3;i>0;i--) {
            try {
                Thread.sleep(3000);
                System.out.println("Второй поток!");
            }catch (InterruptedException e){
            }
        }
        (new Thread(new Hellorunnable())).start();
        System.out.println("Hello from main thread!");
    }
}
