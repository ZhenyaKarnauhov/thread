package ru.kev.readandwrite;

import java.io.*;

/**
 * Класс, в котором реализовано многопоточное считывание данных из файлов из запись в результирующий файл.
 *
 * @author Evgeniy Karnauhov 15IT18.
 */
public class ReadAndWrite extends Thread {
    private String path;
    private static volatile BufferedWriter bufferedWriter;

    public ReadAndWrite(String path) {
        this.path = path;
    }

    public void run() {
        long period = System.currentTimeMillis();
        String string;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            while ((string = bufferedReader.readLine()) != null) {
                record(string);
                yield();
            }
        } catch (IOException e) {
        e.printStackTrace();
        }
        long period1 = System.currentTimeMillis() - period;
        System.out.println("Время потока: "+period1);

    }

    /**
     * Метод для записи строки в файл "Итог.txt"
     *
     * @param string строка,записывающаяся в файл
     * @throws IOException исключение
     */
    public static synchronized void record(String string) throws IOException {
        bufferedWriter.write(string + "\n");
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\kev\\readandwrite\\Итог.txt"));

        ReadAndWrite thread1 = new ReadAndWrite("src\\ru\\kev\\readandwrite\\первый.txt");
        ReadAndWrite thread2 = new ReadAndWrite("src\\ru\\kev\\readandwrite\\второй.txt");

        thread1.start();
        thread2.start();

        if (thread1.isAlive()) {
            thread1.join();
        }
        if (thread2.isAlive()) {
            thread2.join();
        }
        bufferedWriter.close();
    }
}