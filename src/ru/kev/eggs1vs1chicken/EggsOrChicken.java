package ru.kev.eggs1vs1chicken;


/**
 * Класс разрешает спор: "Что появилось сначала - яйцо или курица?"
 *
 * @author Evgeniy Karnauhov 15IT18.
 */

class EggsOrChicken extends Thread {
    public void run() {
        setPriority(10);
        for (int i = 0; i < 20; i++) {
            try {
                sleep(300);
            } catch (InterruptedException e) {
            }
            System.out.println("egg!");
        }
    }
        static EggsOrChicken thread;

        public static void main(String[] args) {
            Thread asd = Thread.currentThread();
            asd.setPriority(10);
            thread = new EggsOrChicken();
            thread.start();
            for (int i = 0; i < 20; i++) {
                try {

                    sleep(300);

                } catch (InterruptedException e) {
                }
                System.out.println("chicken!");
            }
            if (thread.isAlive()) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                }

                System.out.println("First egg");
            } else {
                System.out.println("First chicken!");
            }
        }
    }
